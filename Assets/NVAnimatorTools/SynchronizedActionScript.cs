﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace NV.SynchronizedTasks
{
	public class SynchronizedActionScript : MonoBehaviour {
	
		[SerializeField]
		UnityEvent OnInvoke;
	
		System.Action finalizeAction;
	
		public void Invoke(System.Action initialize, System.Action finalize)
		{
			initialize();
			OnInvoke.Invoke();
			finalizeAction = finalize;
		}
	
		public void Finish()
		{
			finalizeAction();
		}
	}
}
