﻿using UnityEngine;
using System.Collections;

namespace NV.GeneratedParameterManipulation
{
    public class NVClickableAttribute : System.Attribute{}
    
    public class NVClickableIntAttribute : System.Attribute{}
    
    public class NVClickableBoolAttribute : System.Attribute{}
    
    public class NVClickableFloatAttribute : System.Attribute{}
    
    public abstract class BaseReceiverScript : MonoBehaviour {
    }
}
