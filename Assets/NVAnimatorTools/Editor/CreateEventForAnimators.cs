﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditor.Animations;
using System.Text;
using System.IO;
using System.Linq;
using System.Reflection;

public class CreateEventForAnimators : EditorWindow
{
	AnimatorController controller;
	TextAsset sbTemplate;
	TextAsset dpHeaderTemplate;
	TextAsset dpFooterTemplate;
	TextAsset dpStateTemplate;
	TextAsset rcHeaderTemplate;
	TextAsset rcFooterTemplate;
	TextAsset rcBoolParamTemplate;
	TextAsset rcIntParamTemplate;
	TextAsset rcTriggerParamTemplate;
	TextAsset rcFloatParamTemplate;
	bool enableEnter = true;
	bool enableUpdate = false;
	bool enableExit = true;
	bool enableMove = false;
	bool enableIk = false;
	bool overrideOriginalSettings = false;
	bool equipAC;

	public void OnGUI ()
	{
		EditorGUILayout.BeginVertical ();
		if (GUILayout.Button ("Find templates !"))
			FindDefaultTemplates ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Animator Controller to Equip : ");
		controller = (AnimatorController)EditorGUILayout.ObjectField (controller, typeof(AnimatorController), false);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("State Behaviour Template : ");
		sbTemplate = (TextAsset)EditorGUILayout.ObjectField (sbTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Dispatcher Header Template : ");
		dpHeaderTemplate = (TextAsset)EditorGUILayout.ObjectField (dpHeaderTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Dispatcher Footer Template : ");
		dpFooterTemplate = (TextAsset)EditorGUILayout.ObjectField (dpFooterTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Dispatcher State Template : ");
		dpStateTemplate = (TextAsset)EditorGUILayout.ObjectField (dpStateTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();
        
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Receiver Header Template : ");
		rcHeaderTemplate = (TextAsset)EditorGUILayout.ObjectField (rcHeaderTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();
        
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Receiver Footer Template : ");
		rcFooterTemplate = (TextAsset)EditorGUILayout.ObjectField (rcFooterTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();
        
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Receiver Bool Parameter Template : ");
		rcBoolParamTemplate = (TextAsset)EditorGUILayout.ObjectField (rcBoolParamTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();
        
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Receiver Float Parameter Template : ");
		rcFloatParamTemplate = (TextAsset)EditorGUILayout.ObjectField (rcFloatParamTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();
        
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Receiver Trigger Parameter Template : ");
		rcTriggerParamTemplate = (TextAsset)EditorGUILayout.ObjectField (rcTriggerParamTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();
        
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Receiver Int Parameter Template : ");
		rcIntParamTemplate = (TextAsset)EditorGUILayout.ObjectField (rcIntParamTemplate, typeof(TextAsset), false);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Enable Enter : ");
		enableEnter = EditorGUILayout.Toggle (enableEnter);
		EditorGUILayout.EndHorizontal ();
		
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Enable Update : ");
		enableUpdate = EditorGUILayout.Toggle (enableUpdate);
		EditorGUILayout.EndHorizontal ();
		
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Enable Exit : ");
		enableExit = EditorGUILayout.Toggle (enableExit);
		EditorGUILayout.EndHorizontal ();
		
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Enable Move : ");
		enableMove = EditorGUILayout.Toggle (enableMove);
		EditorGUILayout.EndHorizontal ();
		
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Enable Ik : ");
		enableIk = EditorGUILayout.Toggle (enableIk);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.Separator ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Override Previous Settings : ");
		overrideOriginalSettings = EditorGUILayout.Toggle (overrideOriginalSettings);
		EditorGUILayout.EndHorizontal ();

		if (GUILayout.Button ("Generate")) {
			GenerateDispatcher ();
		}

		if (GUILayout.Button ("EquipAnimatorController")) {
			EquipAnimatorController ();
		}

		EditorGUILayout.EndVertical ();
	}

	public void FindDefaultTemplates ()
	{
		var assets = AssetDatabase.FindAssets ("RxStateBehaviourTemplate");
		sbTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;
				
		assets = AssetDatabase.FindAssets ("RxDispatcherHeaderTemplate");
		dpHeaderTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;
				
		assets = AssetDatabase.FindAssets ("DispatcherFooterTemplate");
		dpFooterTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;
		
		assets = AssetDatabase.FindAssets ("RxDispatcherStateTemplate");
		dpStateTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null; 
		
		assets = AssetDatabase.FindAssets ("ReceiverHeaderTemplate");
		rcHeaderTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;
		
		assets = AssetDatabase.FindAssets ("ReceiverFooterTemplate");
		rcFooterTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;
		
		assets = AssetDatabase.FindAssets ("ReceiverBoolParamTemplate");
		rcBoolParamTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;

		assets = AssetDatabase.FindAssets ("ReceiverIntParamTemplate");
		rcIntParamTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;

		assets = AssetDatabase.FindAssets ("ReceiverTriggerParamTemplate");
		rcTriggerParamTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;

		assets = AssetDatabase.FindAssets ("ReceiverFloatParamTemplate");
		rcFloatParamTemplate = assets != null ? AssetDatabase.LoadAssetAtPath<TextAsset> (AssetDatabase.GUIDToAssetPath (assets.FirstOrDefault ())) : null;

	}

	public void GenerateDispatcher ()
	{
		if (controller == null)
			return;

		Directory.CreateDirectory (string.Format (
            "{0}/{1}", Application.dataPath, "AnimatorControllers"));
		Directory.CreateDirectory (string.Format (
            "{0}/{1}/{2}", Application.dataPath, "AnimatorControllers", controller.name.Replace (" ", "")));

		var dispatcher = new StringBuilder (string.Format (dpHeaderTemplate.text,
                                                           controller.name));
        
		var receiver = new StringBuilder (string.Format (rcHeaderTemplate.text,
                                                             controller.name));

		foreach (var p in controller.parameters) {
			switch (p.type) {
			case AnimatorControllerParameterType.Bool:
				{ 
					receiver.AppendFormat (rcBoolParamTemplate.text, p.name.Replace(" ", ""), p.defaultBool.ToString ().ToLower (), p.nameHash);
					break;
				}
			case AnimatorControllerParameterType.Int:
				{
					receiver.AppendFormat (rcIntParamTemplate.text, p.name.Replace(" ", ""), p.defaultInt, p.nameHash);
					break;
				}
			case AnimatorControllerParameterType.Float:
				{
					receiver.AppendFormat (rcFloatParamTemplate.text, p.name.Replace(" ",""), p.defaultFloat, p.nameHash);
					break;
				}
			case AnimatorControllerParameterType.Trigger:
				{
					receiver.AppendFormat (rcTriggerParamTemplate.text, p.name.Replace(" ", ""), null, p.nameHash);
					break;
				}
			default:
				{
					break;
				}
			}
		}

		foreach (var layer in controller.layers) {            
			Directory.CreateDirectory (string.Format (
                "{0}/{1}/{2}/{3}",
                Application.dataPath, "AnimatorControllers", controller.name.Replace (" ", ""), layer.name.Replace (" ", "")));
			GenerateEventForStateMachine (dispatcher, layer.stateMachine, layer, "");
		}

		dispatcher.Append (string.Format (dpFooterTemplate.text, controller.name));
        
		receiver.Append (string.Format (rcFooterTemplate.text, controller.name));

		File.WriteAllText (string.Format (
            "{0}/{1}/{2}/{2}Dispatcher.cs",
            Application.dataPath, "AnimatorControllers", controller.name.Replace (" ", "")),
                           dispatcher.ToString ());
        
		File.WriteAllText (string.Format (
            "{0}/{1}/{2}/{2}Receiver.cs",
            Application.dataPath, "AnimatorControllers", controller.name.Replace (" ", "")),
                           receiver.ToString ());
                          
		AssetDatabase.Refresh ();
	}

	public void GenerateEventForStateMachine (StringBuilder dp, AnimatorStateMachine sm, 
                                             AnimatorControllerLayer layer, string parentSM)
	{
		foreach (var state in sm.states) {

			dp.Append (string.Format (dpStateTemplate.text,
                                    controller.name.Replace (" ", ""),
                                    layer.name.Replace (" ", ""),
                                    parentSM.Replace (" ", ""),
                                    sm.name.Replace (" ", ""),
                                    state.state.name.Replace (" ", "")));

			var fileName = string.Format (
                "{0}/{1}/{2}/{3}/{4}{5}{6}SB.cs",
                Application.dataPath, "AnimatorControllers", 
                controller.name.Replace (" ", ""), layer.name.Replace (" ", ""), parentSM.Replace (" ", ""), sm.name.Replace (" ", ""), state.state.name.Replace (" ", ""));

			var file = string.Format (sbTemplate.text,
                                     controller.name.Replace (" ", ""),
                                     layer.name.Replace (" ", ""),
                                     parentSM.Replace (" ", ""),
                                     sm.name.Replace (" ", ""),
			                          state.state.name.Replace (" ", ""),
			                          "",
			                          "",
			                          "",
			                          "",  
			                          "");

			File.WriteAllText (fileName, file);
			var enterdefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_ENTER{5}",
			                                controller.name.Replace (" ", ""),
			                                layer.name.Replace (" ", ""),
			                                parentSM.Replace (" ", ""),
			                                sm.name.Replace (" ", ""),
			                                state.state.name.Replace (" ", ""), System.Environment.NewLine);
			var updatedefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_UPDATE{5}",
			                                 controller.name.Replace (" ", ""),
			                                 layer.name.Replace (" ", ""),
			                                 parentSM.Replace (" ", ""),
			                                 sm.name.Replace (" ", ""),
			                                 state.state.name.Replace (" ", ""), System.Environment.NewLine);
			var exitdefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_EXIT{5}",
			                               controller.name.Replace (" ", ""),
			                               layer.name.Replace (" ", ""),
			                               parentSM.Replace (" ", ""),
			                               sm.name.Replace (" ", ""),
			                               state.state.name.Replace (" ", ""), System.Environment.NewLine);
			var movedefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_MOVE{5}",
			                               controller.name.Replace (" ", ""),
			                               layer.name.Replace (" ", ""),
			                               parentSM.Replace (" ", ""),
			                               sm.name.Replace (" ", ""),
			                               state.state.name.Replace (" ", ""), System.Environment.NewLine);
			var ikdefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_IK{5}",
			                             controller.name.Replace (" ", ""),
			                             layer.name.Replace (" ", ""),
			                             parentSM.Replace (" ", ""),
			                             sm.name.Replace (" ", ""),
			                             state.state.name.Replace (" ", ""), System.Environment.NewLine);
			dp.Replace (enterdefine, "");
			dp.Replace (updatedefine, "");
			dp.Replace (exitdefine, "");
			dp.Replace (movedefine, "");
			dp.Replace (ikdefine, "");
			dp.Insert (0, enterdefine);
			dp.Insert (0, updatedefine);
			dp.Insert (0, exitdefine);
			dp.Insert (0, movedefine);
			dp.Insert (0, ikdefine);
		}
         
		foreach (var subsm in sm.stateMachines) {
			GenerateEventForStateMachine (dp, subsm.stateMachine, layer, sm.name + "_");
		}
	}

	public void EquipAnimatorController ()
	{
		if (controller == null)
			return;

		var dispatcher = new StringBuilder (File.ReadAllText (string.Format (
			"{0}/{1}/{2}/{2}Dispatcher.cs",
			Application.dataPath, "AnimatorControllers", controller.name.Replace (" ", ""))));
        
		foreach (var layer in controller.layers) { 
			EquipStateMachine (dispatcher, layer.stateMachine, layer, "");
		}

		File.WriteAllText (string.Format (
			"{0}/{1}/{2}/{2}Dispatcher.cs",
			Application.dataPath, "AnimatorControllers", controller.name.Replace (" ", "")),
		                   dispatcher.ToString ());
		AssetDatabase.Refresh ();
	}

	public void EquipStateMachine (StringBuilder dispatcher, AnimatorStateMachine sm, 
                                  AnimatorControllerLayer layer, string parentSM)
	{
		foreach (var state in sm.states) {
			var className = string.Format ("NV.GeneratedEventDispatcher.{0}.{1}.{2}{3}{4}SB",
                                          controller.name.Replace (" ", ""),
                                          layer.name.Replace (" ", ""),
                                          parentSM.Replace (" ", ""),
                                          sm.name.Replace (" ", ""),
                                          state.state.name.Replace (" ", ""));
			var classObject = System.AppDomain.CurrentDomain.GetAssemblies ()
                .Select (a => a.GetType (
                    className))
                    .FirstOrDefault (t => t != null);

			if (classObject != null) {
				var sb = state.state.behaviours.FirstOrDefault (b =>
                                                  b.GetType ().Name == classObject.Name);



				state.state.behaviours = state.state.behaviours.Where (b =>
                                                                  b.GetType ().Name != classObject.Name).ToArray ();

				var nb = state.state.AddStateMachineBehaviour (classObject);
				var bf = BindingFlags.Instance 
					| BindingFlags.NonPublic
					| BindingFlags.Public;
				if (sb != null && !overrideOriginalSettings) {


					classObject.GetField ("enter", bf).SetValue (nb, 
                                                                 classObject.GetField ("enter", bf).GetValue (sb));

            
					classObject.GetField ("exit", bf).SetValue (nb, 
                                                                classObject.GetField ("exit", bf).GetValue (sb));
            
					classObject.GetField ("update", bf).SetValue (nb, 
                                                                  classObject.GetField ("update", bf).GetValue (sb));

					classObject.GetField ("ik", bf).SetValue (nb, 
                                                              classObject.GetField ("ik", bf).GetValue (sb));

					classObject.GetField ("move", bf).SetValue (nb, 
                                                                classObject.GetField ("move", bf).GetValue (sb));
				} else {
					classObject.GetField ("enter", bf).SetValue (nb, enableEnter);
					classObject.GetField ("exit", bf).SetValue (nb, enableExit);
					classObject.GetField ("update", bf).SetValue (nb, enableUpdate);
					classObject.GetField ("ik", bf).SetValue (nb, enableIk);
					classObject.GetField ("move", bf).SetValue (nb, enableMove);
				}

				var fileName = string.Format (
					"{0}/{1}/{2}/{3}/{4}{5}{6}SB.cs",
					Application.dataPath, "AnimatorControllers", 
					controller.name.Replace (" ", ""), layer.name.Replace (" ", ""), parentSM.Replace (" ", ""), sm.name.Replace (" ", ""), state.state.name.Replace (" ", ""));
				bool onenter = ((bool)classObject.GetField ("enter", bf).GetValue (nb));
				bool onupdate = ((bool)classObject.GetField ("update", bf).GetValue (nb));
				bool onexit = ((bool)classObject.GetField ("exit", bf).GetValue (nb));
				bool onmove = ((bool)classObject.GetField ("move", bf).GetValue (nb));
				bool onik = ((bool)classObject.GetField ("ik", bf).GetValue (nb));
				var file = string.Format (sbTemplate.text,
				                          controller.name.Replace (" ", ""),
				                          layer.name.Replace (" ", ""),
				                          parentSM.Replace (" ", ""),
				                          sm.name.Replace (" ", ""),
				                          state.state.name.Replace (" ", ""),
				                          onenter ? "" : "//",
				                          onupdate ? "" : "//",
				                          onexit ? "" : "//",
				                          onmove ? "" : "//", 
				                          onik ? "" : "//");
				
				File.WriteAllText (fileName, file);

				var enterdefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_ENTER{5}",
				                           controller.name.Replace (" ", ""),
				                           layer.name.Replace (" ", ""),
				                           parentSM.Replace (" ", ""),
				                           sm.name.Replace (" ", ""),
				                           state.state.name.Replace (" ", ""), System.Environment.NewLine);
				var updatedefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_UPDATE{5}",
				                                controller.name.Replace (" ", ""),
				                                layer.name.Replace (" ", ""),
				                                parentSM.Replace (" ", ""),
				                                sm.name.Replace (" ", ""),
				                                state.state.name.Replace (" ", ""), System.Environment.NewLine);
				var exitdefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_EXIT{5}",
				                                controller.name.Replace (" ", ""),
				                                layer.name.Replace (" ", ""),
				                                parentSM.Replace (" ", ""),
				                                sm.name.Replace (" ", ""),
				                                state.state.name.Replace (" ", ""), System.Environment.NewLine);
				var movedefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_MOVE{5}",
				                                controller.name.Replace (" ", ""),
				                                layer.name.Replace (" ", ""),
				                                parentSM.Replace (" ", ""),
				                                sm.name.Replace (" ", ""),
				                                state.state.name.Replace (" ", ""), System.Environment.NewLine);
				var ikdefine = string.Format ("#define NV_GENERATEDEVENTDISPATCHER_{0}_{1}_{2}{3}{4}SB_IK{5}",
				                                controller.name.Replace (" ", ""),
				                                layer.name.Replace (" ", ""),
				                                parentSM.Replace (" ", ""),
				                                sm.name.Replace (" ", ""),
				                                state.state.name.Replace (" ", ""), System.Environment.NewLine);
				dispatcher.Replace (enterdefine, "");
				dispatcher.Replace (updatedefine, "");
				dispatcher.Replace (exitdefine, "");
				dispatcher.Replace (movedefine, "");
				dispatcher.Replace (ikdefine, "");

				if (onenter) {
					dispatcher.Insert (0, enterdefine);
				}
				
				if (onupdate) {
					dispatcher.Insert (0, updatedefine);
				}
				
				if (onexit) {
					dispatcher.Insert (0, exitdefine);
				}
				
				if (onmove) {
					dispatcher.Insert (0, movedefine);
				}
				
				if (onik) {
					dispatcher.Insert (0, ikdefine);
				}

				EditorUtility.SetDirty (state.state);
			}

		}
		foreach (var subsm in sm.stateMachines) {
			EquipStateMachine (dispatcher, subsm.stateMachine, layer, sm.name + "_");
		}
	}

	[MenuItem("Custom/CreateEventForAnimators")]
	public static void CreateWindow ()
	{
		var window = ScriptableObject.CreateInstance<CreateEventForAnimators> ();

		window.Show ();
	}
}
