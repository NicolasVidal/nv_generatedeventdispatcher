﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;

namespace NV.GeneratedParameterManipulation
{
    [CustomEditor(typeof (BaseReceiverScript), true)]
    public class BaseReceiverCustomInspector : Editor {
    
        public int[] intParameters; 
        
        public bool[] boolParameters;
        
        public float[] floatParameters;

        public bool actionToggle;

        public override void OnInspectorGUI ()
        {
            base.OnInspectorGUI ();
    
            EditorGUILayout.Separator();
            actionToggle = EditorGUILayout.BeginToggleGroup("For Debug Purposes", actionToggle);

            var type = target.GetType();
    
            EditorGUILayout.BeginVertical();
    
            var methods = type.GetMethods(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public)
                .OrderBy(m => m.Name).ToArray();

            if (intParameters == null || intParameters.Length != methods.Length)
            {
                intParameters = new int[methods.Length];
                boolParameters = new bool[methods.Length];
                floatParameters = new float[methods.Length];
            }
            var i = 0;
            foreach (var m in methods)
            {
                if (m.GetCustomAttributes(typeof (NVClickableAttribute), true).Length != 0)
                {
                    if (GUILayout.Button(m.Name))
                    {
                        m.Invoke(target, null);
                    }
                }
                
                if (m.GetCustomAttributes(typeof (NVClickableIntAttribute), true).Length != 0)
                {
                    EditorGUILayout.BeginHorizontal();
                    if (GUILayout.Button(m.Name))
                    {
                        m.Invoke(target, new object[]{intParameters[i]});
                    }
                    intParameters[i] = EditorGUILayout.IntField(intParameters[i]);

                    EditorGUILayout.EndHorizontal();
                }

                if (m.GetCustomAttributes(typeof (NVClickableBoolAttribute), true).Length != 0)
                {
                    EditorGUILayout.BeginHorizontal();
                    if (GUILayout.Button(m.Name))
                    {
                        m.Invoke(target, new object[]{boolParameters[i]});
                    }
                    boolParameters[i] = EditorGUILayout.Toggle(boolParameters[i]);
                    
                    EditorGUILayout.EndHorizontal();
                }

                if (m.GetCustomAttributes(typeof (NVClickableFloatAttribute), true).Length != 0)
                {
                    EditorGUILayout.BeginHorizontal();
                    if (GUILayout.Button(m.Name))
                    {
                        m.Invoke(target, new object[]{floatParameters[i]});
                    }
                    floatParameters[i] = EditorGUILayout.FloatField(floatParameters[i]);
                    
                    EditorGUILayout.EndHorizontal();
                }
                i++;
            }
            EditorGUILayout.EndToggleGroup();
            EditorGUILayout.EndVertical();
        }
    }
}