﻿        #region {0}
        
        [NVClickableBool]
        public void {0}Set (bool v)
        {{
            animator.SetBool({2}, v);
        }}

        [NVClickable]
        public void {0}SetTrue ()
        {{
            {0}Set (true);
        }}

        [NVClickable]
        public void {0}SetFalse ()
        {{
            {0}Set (false);
        }}
        
        [NVClickable]
        public void {0}Reset ()
        {{
            {0}Set ({1});
        }}
        
        public bool {0}Get ()
        {{
            return animator.GetBool({2});
        }}
        
        #endregion
