﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace NV.SynchronizedTasks
{
	[System.Serializable]
	public class UnityEventInitAndFinish : UnityEvent<System.Action, System.Action> {}

	public class WaitAllExtentionScript : MonoBehaviour {
	
		[SerializeField]
		UnityEventInitAndFinish evt;

		[SerializeField]
		UnityEvent OnFinished;

		int counter = 0;

		public void WaitAll()
		{	evt.Invoke(() => counter += 1,
			           () => {
				counter -= 1;
				if (counter == 0)
				{
					OnFinished.Invoke();
				}
			});
		}
	}
}
